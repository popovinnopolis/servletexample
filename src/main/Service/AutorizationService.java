package Service;


import Model.dao.UserDao;

/**
 * Created by admin on 01.07.2017.
 */
public class AutorizationService {
    public static boolean auth(String login, String password) {
        return UserDao.getUserByLoginAndPassword(login, password) != null;

    }
}